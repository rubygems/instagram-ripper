# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'instagram/ripper/version'

Gem::Specification.new do |spec|
  spec.name          = "instagram-ripper"
  spec.version       = Instagram::Ripper::VERSION
  spec.authors       = ["Ricardo Ichizo"]
  spec.email         = ["ricardo.ichizo@gmail.com"]

  spec.summary       = %q{Just a simple ruby gem to load an entire instagram page at one time!}
  spec.description   = %q{Just a simple ruby gem to load an entire instagram page at one time! It has not any association with Instagram.com, Facebook.com or their creators.}
  spec.homepage      = "https://gitlab.com/rubygems/instagram-ripper"
  spec.license       = "Apache-2.0"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.12"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"

  spec.add_runtime_dependency "poltergeist", "~> 1.10"
  spec.add_runtime_dependency "capybara", "~> 2.8"
  spec.add_runtime_dependency "nokogiri", "~> 1.6"
end
