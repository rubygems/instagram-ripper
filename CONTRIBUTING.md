Help us to improvise the gem. The motivation to create this piece of code was due the inefficiency of the Instagram API.

To contribute just follow the steps below:

1. Fork it
2. Create your feature branch (git checkout -b my-new-feature)
3. Commit your changes (git commit -am 'Add some feature')
4. Push to the branch (git push origin my-new-feature)
5. Create new Merge Request
